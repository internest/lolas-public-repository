# LoLas Public Repository #
![Internest logo](https://media-exp1.licdn.com/dms/image/C560BAQHGO-wuGorArw/company-logo_200_200/0?e=2159024400&v=beta&t=uowA5Tn-3fGQHzqmqsS5r0-mZ78rQxrb-J7dlEHRSuY)

Welcome to Internest LoLas public repository gathering all the ressources you may need to use our system.

### What can you find here? ###

* Documents : all the up-to-date documents you have access to as you bought LoLas NG
* Tools : all the freeware tools we offer to LoLas users 
* Sample codes : codes that could ease your integration, such as communication parser or guidance demo code.

### How to download the files? ###
Either you can browse through the repository for the document you are looking for and download it by cliking on "view raw", 

![How to download a single doc image](Repository_images/HowToDownloadImage_SingleDoc.png "Download a single doc")

or you can download the full repository using the "Downloads" section in the left tab.

![How to download all docs image](Repository_images/HowToDownloadImage_AllDocs.png "Download all documents")

### Contact ###

You can contact Internest support team at support@internest.fr