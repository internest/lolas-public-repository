#pragma once
// MESSAGE internest_msg_position PACKING

#define MAVLINK_MSG_ID_internest_msg_position 238

MAVPACKED(
typedef struct __mavlink_internest_msg_position_t {
 uint32_t time_boot_ms; /*< */
 int32_t x; /*< x position in mm in the local frame*/
 int32_t y; /*< y position in mm in the local frame*/
 int32_t z; /*< z position in mm in the local frame*/
 int32_t a; /*< yaw angle in degres in the local frame */
 uint16_t status; /*<list of booleans containing hardware and software status information */
 uint16_t standardDeviationXY; /*< horizontal standard deviation */
 uint16_t standardDeviationZ; /*< vertical standard deviation*/
 uint16_t standardDeviationA; /*< angle standard deviation*/
}) mavlink_internest_msg_position_t;

#define MAVLINK_MSG_ID_internest_msg_position_LEN 28
#define MAVLINK_MSG_ID_internest_msg_position_MIN_LEN 28
#define MAVLINK_MSG_ID_238_LEN 28
#define MAVLINK_MSG_ID_238_MIN_LEN 28

#define MAVLINK_MSG_ID_internest_msg_position_CRC 150
#define MAVLINK_MSG_ID_238_CRC 150



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_internest_msg_position { \
    238, \
    "internest_msg_position", \
    9, \
    {  { "time_boot_ms", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_internest_msg_position_t, time_boot_ms) }, \
         { "x", NULL, MAVLINK_TYPE_INT32_T, 0, 4, offsetof(mavlink_internest_msg_position_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_internest_msg_position_t, y) }, \
         { "z", NULL, MAVLINK_TYPE_INT32_T, 0, 12, offsetof(mavlink_internest_msg_position_t, z) }, \
         { "a", NULL, MAVLINK_TYPE_INT32_T, 0, 16, offsetof(mavlink_internest_msg_position_t, a) }, \
         { "status", NULL, MAVLINK_TYPE_UINT16_T, 0, 20, offsetof(mavlink_internest_msg_position_t, status) }, \
         { "standardDeviationXY", NULL, MAVLINK_TYPE_UINT16_T, 0, 22, offsetof(mavlink_internest_msg_position_t, standardDeviationXY) }, \
         { "standardDeviationZ", NULL, MAVLINK_TYPE_UINT16_T, 0, 24, offsetof(mavlink_internest_msg_position_t, standardDeviationZ) }, \
         { "standardDeviationA", NULL, MAVLINK_TYPE_UINT16_T, 0, 26, offsetof(mavlink_internest_msg_position_t, standardDeviationA) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_internest_msg_position { \
    "internest_msg_position", \
    9, \
    {  { "time_boot_ms", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_internest_msg_position_t, time_boot_ms) }, \
         { "x", NULL, MAVLINK_TYPE_INT32_T, 0, 4, offsetof(mavlink_internest_msg_position_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_internest_msg_position_t, y) }, \
         { "z", NULL, MAVLINK_TYPE_INT32_T, 0, 12, offsetof(mavlink_internest_msg_position_t, z) }, \
         { "a", NULL, MAVLINK_TYPE_INT32_T, 0, 16, offsetof(mavlink_internest_msg_position_t, a) }, \
         { "status", NULL, MAVLINK_TYPE_UINT16_T, 0, 20, offsetof(mavlink_internest_msg_position_t, status) }, \
         { "standardDeviationXY", NULL, MAVLINK_TYPE_UINT16_T, 0, 22, offsetof(mavlink_internest_msg_position_t, standardDeviationXY) }, \
         { "standardDeviationZ", NULL, MAVLINK_TYPE_UINT16_T, 0, 24, offsetof(mavlink_internest_msg_position_t, standardDeviationZ) }, \
         { "standardDeviationA", NULL, MAVLINK_TYPE_UINT16_T, 0, 26, offsetof(mavlink_internest_msg_position_t, standardDeviationA) }, \
         } \
}
#endif

/**
 * @brief Pack a internest_msg_position message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time_boot_ms 
 * @param status 
 * @param x 
 * @param y 
 * @param z 
 * @param a 
 * @param standardDeviationXY 
 * @param standardDeviationZ 
 * @param standardDeviationA 
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_internest_msg_position_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time_boot_ms, uint16_t status, int32_t x, int32_t y, int32_t z, int32_t a, uint16_t standardDeviationXY, uint16_t standardDeviationZ, uint16_t standardDeviationA)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_internest_msg_position_LEN];
    _mav_put_uint32_t(buf, 0, time_boot_ms);
    _mav_put_int32_t(buf, 4, x);
    _mav_put_int32_t(buf, 8, y);
    _mav_put_int32_t(buf, 12, z);
    _mav_put_int32_t(buf, 16, a);
    _mav_put_uint16_t(buf, 20, status);
    _mav_put_uint16_t(buf, 22, standardDeviationXY);
    _mav_put_uint16_t(buf, 24, standardDeviationZ);
    _mav_put_uint16_t(buf, 26, standardDeviationA);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_internest_msg_position_LEN);
#else
    mavlink_internest_msg_position_t packet;
    packet.time_boot_ms = time_boot_ms;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.a = a;
    packet.status = status;
    packet.standardDeviationXY = standardDeviationXY;
    packet.standardDeviationZ = standardDeviationZ;
    packet.standardDeviationA = standardDeviationA;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_internest_msg_position_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_internest_msg_position;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_internest_msg_position_MIN_LEN, MAVLINK_MSG_ID_internest_msg_position_LEN, MAVLINK_MSG_ID_internest_msg_position_CRC);
}

/**
 * @brief Pack a internest_msg_position message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time_boot_ms 
 * @param status 
 * @param x 
 * @param y 
 * @param z 
 * @param a 
 * @param standardDeviationXY 
 * @param standardDeviationZ 
 * @param standardDeviationA 
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_internest_msg_position_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time_boot_ms,uint16_t status,int32_t x,int32_t y,int32_t z,int32_t a,uint16_t standardDeviationXY,uint16_t standardDeviationZ,uint16_t standardDeviationA)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_internest_msg_position_LEN];
    _mav_put_uint32_t(buf, 0, time_boot_ms);
    _mav_put_int32_t(buf, 4, x);
    _mav_put_int32_t(buf, 8, y);
    _mav_put_int32_t(buf, 12, z);
    _mav_put_int32_t(buf, 16, a);
    _mav_put_uint16_t(buf, 20, status);
    _mav_put_uint16_t(buf, 22, standardDeviationXY);
    _mav_put_uint16_t(buf, 24, standardDeviationZ);
    _mav_put_uint16_t(buf, 26, standardDeviationA);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_internest_msg_position_LEN);
#else
    mavlink_internest_msg_position_t packet;
    packet.time_boot_ms = time_boot_ms;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.a = a;
    packet.status = status;
    packet.standardDeviationXY = standardDeviationXY;
    packet.standardDeviationZ = standardDeviationZ;
    packet.standardDeviationA = standardDeviationA;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_internest_msg_position_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_internest_msg_position;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_internest_msg_position_MIN_LEN, MAVLINK_MSG_ID_internest_msg_position_LEN, MAVLINK_MSG_ID_internest_msg_position_CRC);
}

/**
 * @brief Encode a internest_msg_position struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param internest_msg_position C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_internest_msg_position_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_internest_msg_position_t* internest_msg_position)
{
    return mavlink_msg_internest_msg_position_pack(system_id, component_id, msg, internest_msg_position->time_boot_ms, internest_msg_position->status, internest_msg_position->x, internest_msg_position->y, internest_msg_position->z, internest_msg_position->a, internest_msg_position->standardDeviationXY, internest_msg_position->standardDeviationZ, internest_msg_position->standardDeviationA);
}

/**
 * @brief Encode a internest_msg_position struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param internest_msg_position C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_internest_msg_position_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_internest_msg_position_t* internest_msg_position)
{
    return mavlink_msg_internest_msg_position_pack_chan(system_id, component_id, chan, msg, internest_msg_position->time_boot_ms, internest_msg_position->status, internest_msg_position->x, internest_msg_position->y, internest_msg_position->z, internest_msg_position->a, internest_msg_position->standardDeviationXY, internest_msg_position->standardDeviationZ, internest_msg_position->standardDeviationA);
}

/**
 * @brief Send a internest_msg_position message
 * @param chan MAVLink channel to send the message
 *
 * @param time_boot_ms 
 * @param status 
 * @param x 
 * @param y 
 * @param z 
 * @param a 
 * @param standardDeviationXY 
 * @param standardDeviationZ 
 * @param standardDeviationA 
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_internest_msg_position_send(mavlink_channel_t chan, uint32_t time_boot_ms, uint16_t status, int32_t x, int32_t y, int32_t z, int32_t a, uint16_t standardDeviationXY, uint16_t standardDeviationZ, uint16_t standardDeviationA)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_internest_msg_position_LEN];
    _mav_put_uint32_t(buf, 0, time_boot_ms);
    _mav_put_int32_t(buf, 4, x);
    _mav_put_int32_t(buf, 8, y);
    _mav_put_int32_t(buf, 12, z);
    _mav_put_int32_t(buf, 16, a);
    _mav_put_uint16_t(buf, 20, status);
    _mav_put_uint16_t(buf, 22, standardDeviationXY);
    _mav_put_uint16_t(buf, 24, standardDeviationZ);
    _mav_put_uint16_t(buf, 26, standardDeviationA);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_internest_msg_position, buf, MAVLINK_MSG_ID_internest_msg_position_MIN_LEN, MAVLINK_MSG_ID_internest_msg_position_LEN, MAVLINK_MSG_ID_internest_msg_position_CRC);
#else
    mavlink_internest_msg_position_t packet;
    packet.time_boot_ms = time_boot_ms;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.a = a;
    packet.status = status;
    packet.standardDeviationXY = standardDeviationXY;
    packet.standardDeviationZ = standardDeviationZ;
    packet.standardDeviationA = standardDeviationA;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_internest_msg_position, (const char *)&packet, MAVLINK_MSG_ID_internest_msg_position_MIN_LEN, MAVLINK_MSG_ID_internest_msg_position_LEN, MAVLINK_MSG_ID_internest_msg_position_CRC);
#endif
}

/**
 * @brief Send a internest_msg_position message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_internest_msg_position_send_struct(mavlink_channel_t chan, const mavlink_internest_msg_position_t* internest_msg_position)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_internest_msg_position_send(chan, internest_msg_position->time_boot_ms, internest_msg_position->status, internest_msg_position->x, internest_msg_position->y, internest_msg_position->z, internest_msg_position->a, internest_msg_position->standardDeviationXY, internest_msg_position->standardDeviationZ, internest_msg_position->standardDeviationA);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_internest_msg_position, (const char *)internest_msg_position, MAVLINK_MSG_ID_internest_msg_position_MIN_LEN, MAVLINK_MSG_ID_internest_msg_position_LEN, MAVLINK_MSG_ID_internest_msg_position_CRC);
#endif
}

#if MAVLINK_MSG_ID_internest_msg_position_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_internest_msg_position_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time_boot_ms, uint16_t status, int32_t x, int32_t y, int32_t z, int32_t a, uint16_t standardDeviationXY, uint16_t standardDeviationZ, uint16_t standardDeviationA)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time_boot_ms);
    _mav_put_int32_t(buf, 4, x);
    _mav_put_int32_t(buf, 8, y);
    _mav_put_int32_t(buf, 12, z);
    _mav_put_int32_t(buf, 16, a);
    _mav_put_uint16_t(buf, 20, status);
    _mav_put_uint16_t(buf, 22, standardDeviationXY);
    _mav_put_uint16_t(buf, 24, standardDeviationZ);
    _mav_put_uint16_t(buf, 26, standardDeviationA);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_internest_msg_position, buf, MAVLINK_MSG_ID_internest_msg_position_MIN_LEN, MAVLINK_MSG_ID_internest_msg_position_LEN, MAVLINK_MSG_ID_internest_msg_position_CRC);
#else
    mavlink_internest_msg_position_t *packet = (mavlink_internest_msg_position_t *)msgbuf;
    packet->time_boot_ms = time_boot_ms;
    packet->x = x;
    packet->y = y;
    packet->z = z;
    packet->a = a;
    packet->status = status;
    packet->standardDeviationXY = standardDeviationXY;
    packet->standardDeviationZ = standardDeviationZ;
    packet->standardDeviationA = standardDeviationA;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_internest_msg_position, (const char *)packet, MAVLINK_MSG_ID_internest_msg_position_MIN_LEN, MAVLINK_MSG_ID_internest_msg_position_LEN, MAVLINK_MSG_ID_internest_msg_position_CRC);
#endif
}
#endif

#endif

// MESSAGE internest_msg_position UNPACKING


/**
 * @brief Get field time_boot_ms from internest_msg_position message
 *
 * @return 
 */
static inline uint32_t mavlink_msg_internest_msg_position_get_time_boot_ms(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field status from internest_msg_position message
 *
 * @return 
 */
static inline uint16_t mavlink_msg_internest_msg_position_get_status(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  20);
}

/**
 * @brief Get field x from internest_msg_position message
 *
 * @return 
 */
static inline int32_t mavlink_msg_internest_msg_position_get_x(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  4);
}

/**
 * @brief Get field y from internest_msg_position message
 *
 * @return 
 */
static inline int32_t mavlink_msg_internest_msg_position_get_y(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  8);
}

/**
 * @brief Get field z from internest_msg_position message
 *
 * @return 
 */
static inline int32_t mavlink_msg_internest_msg_position_get_z(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  12);
}

/**
 * @brief Get field a from internest_msg_position message
 *
 * @return 
 */
static inline int32_t mavlink_msg_internest_msg_position_get_a(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  16);
}

/**
 * @brief Get field standardDeviationXY from internest_msg_position message
 *
 * @return 
 */
static inline uint16_t mavlink_msg_internest_msg_position_get_standardDeviationXY(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  22);
}

/**
 * @brief Get field standardDeviationZ from internest_msg_position message
 *
 * @return 
 */
static inline uint16_t mavlink_msg_internest_msg_position_get_standardDeviationZ(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  24);
}

/**
 * @brief Get field standardDeviationA from internest_msg_position message
 *
 * @return 
 */
static inline uint16_t mavlink_msg_internest_msg_position_get_standardDeviationA(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  26);
}

/**
 * @brief Decode a internest_msg_position message into a struct
 *
 * @param msg The message to decode
 * @param internest_msg_position C-struct to decode the message contents into
 */
static inline void mavlink_msg_internest_msg_position_decode(const mavlink_message_t* msg, mavlink_internest_msg_position_t* internest_msg_position)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    internest_msg_position->time_boot_ms = mavlink_msg_internest_msg_position_get_time_boot_ms(msg);
    internest_msg_position->x = mavlink_msg_internest_msg_position_get_x(msg);
    internest_msg_position->y = mavlink_msg_internest_msg_position_get_y(msg);
    internest_msg_position->z = mavlink_msg_internest_msg_position_get_z(msg);
    internest_msg_position->a = mavlink_msg_internest_msg_position_get_a(msg);
    internest_msg_position->status = mavlink_msg_internest_msg_position_get_status(msg);
    internest_msg_position->standardDeviationXY = mavlink_msg_internest_msg_position_get_standardDeviationXY(msg);
    internest_msg_position->standardDeviationZ = mavlink_msg_internest_msg_position_get_standardDeviationZ(msg);
    internest_msg_position->standardDeviationA = mavlink_msg_internest_msg_position_get_standardDeviationA(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_internest_msg_position_LEN? msg->len : MAVLINK_MSG_ID_internest_msg_position_LEN;
        memset(internest_msg_position, 0, MAVLINK_MSG_ID_internest_msg_position_LEN);
    memcpy(internest_msg_position, _MAV_PAYLOAD(msg), len);
#endif
}
